apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: transmission-wireguard
  name: transmission-wireguard
  namespace: media-server
spec:
  replicas: 1
  selector:
    matchLabels:
      app: transmission-wireguard
  template:
    metadata:
      labels:
        app: transmission-wireguard
    spec:
      initContainers:
        - name: transmission-settings-setup
          image: busybox
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: media
              mountPath: /config
              subPath: transmission-config
            - name: transmission-settings
              mountPath: /settings
          command:
            [
              "/bin/sh",
              "-c",
              "cat /settings/settings.json > /config/settings.json",
            ]
          env:
            - name: PUID
              value: "0"
            - name: PGID
              value: "0"
        - name: transmission-postprocess-setup
          image: busybox
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: media
              mountPath: /config
              subPath: transmission-config
            - name: transmission-postprocess
              mountPath: /postprocess
          command:
            [
              "/bin/sh",
              "-c",
              "cat /postprocess/transmission-postprocess.sh > /config/transmission-postprocess.sh && chmod +x /config/transmission-postprocess.sh",
            ]
          env:
            - name: PUID
              value: "0"
            - name: PGID
              value: "0"
      containers:
        - name: pia-wireguard
          image: thrnz/docker-wireguard-pia
          imagePullPolicy: IfNotPresent
          env:
            - name: USER
              valueFrom:
                secretKeyRef:
                  name: pia-secrets
                  key: USER
            - name: PASS
              valueFrom:
                secretKeyRef:
                  name: pia-secrets
                  key: PASS
            - name: PIA_DIP_TOKEN
              valueFrom:
                secretKeyRef:
                  name: pia-secrets
                  key: PIA_DIP_TOKEN
            - name: KEEPALIVE
              value: "25"
            - name: PORT_FORWARDING
              value: "1"
            - name: FIREWALL
              value: "1"
            #- name: LOCAL_NETWORK
            #  value: "10.0.0.0/8"
            - name: POST_UP
              value: "iptables -A INPUT -s 10.0.0.0/8 -i eth0 -p tcp --dport 9091 -j ACCEPT && iptables -A OUTPUT -d 10.0.0.0/8 -o eth0 -p tcp --dport 9091 -j ACCEPT && iptables -A INPUT -s 10.0.0.0/8 -i eth0 -p tcp --sport 9091 -j ACCEPT && iptables -A OUTPUT -d 10.0.0.0/8 -o eth0 -p tcp --sport 9091 -j ACCEPT"
          ports:
            - containerPort: 51820
              name: wireguard
              protocol: UDP
          securityContext:
            capabilities:
              add:
                - NET_ADMIN
          volumeMounts:
            - name: wireguard-postprocess
              mountPath: /postprocess
        - name: transmission
          image: lscr.io/linuxserver/transmission:latest
          imagePullPolicy: IfNotPresent
          ports:
            - containerPort: 9091
              name: transmission-ui
              protocol: TCP
          volumeMounts:
            - name: media
              mountPath: /watch
              subPath: transmission-watch
            - name: media
              mountPath: /downloads
              subPath: transmission-downloads
            - name: media
              mountPath: /output
              subPath: filebot-watch
            - name: media
              mountPath: /config
              subPath: transmission-config
            - name: media
              mountPath: /holding
              subPath: filebot-holding
          env:
            - name: PUID
              value: "0"
            - name: PGID
              value: "0"
        - name: ubuntu
          image: ubuntu
          imagePullPolicy: IfNotPresent
          command: [ "/bin/bash", "-c", "--" ]
          args: [ "while true; do sleep 30; done;" ]
          volumeMounts:
            - name: media
              mountPath: /old
              subPath: media
      volumes:
        - name: transmission-postprocess
          configMap:
            name: transmission-postprocess
        - name: transmission-settings
          configMap:
            name: transmission-settings-config
        - name: wireguard-postprocess
          configMap:
            name: wireguard-postprocess
        - name: media
          persistentVolumeClaim:
            claimName: media-pvc
      securityContext:
        sysctls:
          - name: net.ipv4.conf.all.src_valid_mark
            value: "1"
          - name: net.ipv6.conf.all.disable_ipv6
            value: "1"
          - name: net.ipv6.conf.default.disable_ipv6
            value: "1"
          - name: net.ipv6.conf.lo.disable_ipv6
            value: "1"
