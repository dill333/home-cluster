#!/bin/zsh
num_servers=`ansible-inventory --list | jq ".k8s_servers.[] | length"`

echo $num_servers

for i in {0..$((num_servers-1))}
do
    name=`ansible-inventory --list | jq ".k8s_servers.[][$i]"`
    nodename=`echo $name | tr _ - | tr -d '"'`

    echo "$name starting"
    kubectl drain --ignore-daemonsets --delete-emptydir-data $nodename --timeout=2m
    kubectl drain --ignore-daemonsets --delete-emptydir-data $nodename --disable-eviction
    ansible-playbook playbooks/update.yaml --extra-vars "variable_hosts=$name"
    ansible-playbook playbooks/reboot.yaml --extra-vars "variable_hosts=$name"
    kubectl wait --for=condition=Ready node/$nodename --timeout=20m
    kubectl uncordon $nodename
    echo "$name complete"
done